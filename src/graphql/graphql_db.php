<?php

require_once __DIR__ . '/vendor/autoload.php';

use App\DB;
use App\Types;
use GraphQL\GraphQL;
use \GraphQL\Type\Schema;

try {
	// Настройки подключения к БД
	$config = [
		'host' => 'mysql',
		'database' => 'root',
		'username' => 'root',
		'password' => 'root'
	];

	// Инициализация соединения с БД
	DB::init($config);

	// Получение запроса
	$rawInput = file_get_contents('php://input');
	$input = json_decode($rawInput, true);
	$query = $input['query'];
	$variables = $input['variables'] ?? null;

	// Создание схемы
	$schema = new Schema([
		'query' => Types::query()
	]);

	// Выполнение запроса
	$result = GraphQL::executeQuery($schema, $query, null, null, $variables )->toArray();
} catch (\Exception $e) {
	$result = [
		'error' => [
			'message' => $e->getMessage()
		]
	];
}

// Вывод результата
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($result);
