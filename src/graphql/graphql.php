<?php
//http://127.0.0.1:8089/graphql/
//query {hello}

require_once __DIR__ . '/vendor/autoload.php';

use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ObjectType;

try {
	// Получение запроса
	$rawInput = file_get_contents('php://input');
	$input = json_decode($rawInput, true);
	$query = $input['query'];
	$variables = $input['variables'] ?? null;

	// Содание типа данных "Запрос"
	$queryType = new ObjectType([
		'name' => 'Query',
		'fields' => [
			'hello' => [
				'type' => Type::string(),
				'description' => 'Описание',
				'resolve' => function () {
					return 'Hello world';
				}
			],
			'integer' => [
				'type' => Type::int(),
				'resolve' => function () {
					return 5;
				}
			],
			'helloArgs' => [
				'type' => Type::string(),
				'args' => [
					'hello' => Type::string()
				],
				'resolve' => function ($root, $args) {
					return 'Hello world ' . reset($args);
				}
			],
		]
	]);

	// Создание схемы
	$schema = new Schema([
		'query' => $queryType
	]);

	// Выполнение запроса
	$result = GraphQL::executeQuery($schema, $query, null, null , $variables)->toArray();
} catch (\Exception $e) {
	$result = [
		'error' => [
			'message' => $e->getMessage()
		]
	];
}

// Вывод результата
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($result);